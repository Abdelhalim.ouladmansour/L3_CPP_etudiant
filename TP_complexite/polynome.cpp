#include <algorithm>
#include <iostream>
#include <random>
#include <vector>

using namespace std;

int main() {

    const int N1 = 10;
    const int SEED = 0;

    mt19937 gen(SEED);
    uniform_real_distribution<double> dist(0.0, 1.0);
    auto myrand = [&gen, &dist] () { return dist(gen); };

    vector<double> p(N1);
    generate(p.begin(), p.end(), myrand);

    for (double x : p)
        cout << " " << x;
    cout << endl;

    return 0;
}

