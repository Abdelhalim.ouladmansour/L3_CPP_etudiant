#include <algorithm>
#include <iostream>
#include <vector>

bool search(const std::vector<int> & v, int x) {
    return v.end() != std::find(v.begin(), v.end(), x);
}

void print(const std::vector<int> & v) {
    std::cout << "vector:";
    for (int x : v)
        std::cout << " " << x;
    std::cout << std::endl;
}

